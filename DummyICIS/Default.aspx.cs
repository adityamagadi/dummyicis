﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace DummyICIS
{
    public partial class _Default : Page
    {
        public string query = "InsertErrorLogs";
        //public SqlConnection conn = new SqlConnection("Server=f80c9d2a-0725-4272-b139-a3df0076e35b.sqlserver.sequelizer.com;Database=dbf80c9d2a07254272b139a3df0076e35b;User ID=tlczthzdfpiufmgu;Password=fHfLEVSdiXmwe3fyfx3cumVSQph7AoWpesXtUeaaSbU6fFctE4RWHnA2Uv4pMUK4;");
        public SqlConnection conn = new SqlConnection("Server=e8125e04-bf67-4d94-ba4e-a3e5013008e0.sqlserver.sequelizer.com;Database=dbe8125e04bf674d94ba4ea3e5013008e0;User ID=czzwayaqsctcakde;Password=PVzRYRQDXBjXT6KdSJsdo6j6wZe3tEky5wysTedwnxeFWm3UCSwyhc4RKBt8MVkr;");
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnWebUI_Click(object sender, EventArgs e)
        {
            
            SqlCommand cmd = new SqlCommand(query,conn);
            cmd.Parameters.Add("@ErrorType", SqlDbType.VarChar).Value = "WEB";
            
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            cmd.Dispose();
        }

        protected void btnWinserv_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.Add("@ErrorType", SqlDbType.VarChar).Value = "WS";

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            cmd.Dispose();
        }

        protected void btnWebServ_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.Add("@ErrorType", SqlDbType.VarChar).Value = "Webser";

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            cmd.Dispose();
        }
    }
}