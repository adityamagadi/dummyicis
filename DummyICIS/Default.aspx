﻿<%@ Page Title="DummyICIS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DummyICIS._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                
            </hgroup>
            <p>
                Click the button to generate the respective errors.
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <asp:Button ID="btnWebUI" runat="server" Text ="Click this button to add Web UI error" OnClick="btnWebUI_Click"/>
    <br />
    <br />
    <br />
    <br />
    <asp:Button ID="btnWinserv" runat="server" Text ="Click this button to add Windows service error" OnClick="btnWinserv_Click"/>
    <br />
    <br />
    <br />
    <br />
    <asp:Button ID="btnWebServ" runat="server" Text ="Click this button to add Web Service error" OnClick="btnWebServ_Click"/>
 </asp:Content>
